module.exports = function(){
  it('must be able to nofity that the system is waiting for a comparison between two elements', async function(){
    await this.adapter.notifySystemIsWaitingForComparison({id1: "id1", id2: "id2"})
  })
}

module.exports = function({state, notifier}){
  return Object.freeze({
    insertElement,
    getElements,
    setComparisonResult
  })

  async function insertElement(id){
    const elements = await state.getElements()
    if(elements.length === 0){
      await state.addElement(id)
    }else{
      await insertElementOnSubList(elements, 0, elements.length, id)
    }
  }

  async function insertElementOnSubList(list, firstPosition, lastPosition, id){
    if((lastPosition - firstPosition) === 0){
      await state.insertElementAt(id, lastPosition)
    }else{
      const middlePosition = await getMiddlePosition(firstPosition, lastPosition)
      const middleElement = list[middlePosition]
      await state.setWaitingForComparison(middleElement, id, true)
      await notifier.notifySystemIsWaitingForComparison({id1: id, id2: middleElement})
    }
  }

  async function getMiddlePosition(firstPosition, lastPosition){
    return (Math.floor((lastPosition-firstPosition-1) / 2))+firstPosition
  }

  async function getElements(){
    return state.getElements()
  }

  async function getElementInTheMiddle(){
    const elements = await state.getElements()
    var middlePosition = (Math.floor((elements.length-1) / 2))
    return elements[middlePosition]
  }

  async function setComparisonResult({id1, id2, result}){
    var isExpectedComparison = await state.isWaitingForComparison(id1, id2)
    if(!isExpectedComparison) return

    await state.setComparisonResult({id1, id2, result})

    var newElement = await getTheNewElementToInsert(id1,id2)
    if(!newElement) return
    
    if(newElement === id1){
      if(result === 'greater'){
        const elements = await state.getElements()
        var lastPosition = elements.indexOf(id2)
        var firstPosition = await findHigherPriorityElementFromPosition(id1, lastPosition)
        await insertElementOnSubList(elements, firstPosition, lastPosition, id1)
      } else if(result === 'lower'){
          const elements = await state.getElements()
          var firstPosition = elements.indexOf(id2) + 1
          var lastPosition = await findLowerPriorityElementFromPosition(id1, firstPosition)
          await insertElementOnSubList(elements, firstPosition, lastPosition, id1)
      } else if(result === 'equal') await state.insertElementAfter(id1, id2)
    }else if(newElement === id2){
      if(result == 'equal') await state.insertElementAfter(id2, id1)
      else if(result === 'greater') {
        const elements = await state.getElements()
        var firstPosition = elements.indexOf(id1) + 1
        var lastPosition = await findLowerPriorityElementFromPosition(id2, firstPosition)
        await insertElementOnSubList(elements, firstPosition, lastPosition, id2)
      } else if(result === 'lower'){
        const elements = await state.getElements()
        var lastPosition = elements.indexOf(id1)
        var firstPosition = await findHigherPriorityElementFromPosition(id2, lastPosition)
        await insertElementOnSubList(elements, firstPosition, lastPosition, id2)
      }
    }
  }

  async function findHigherPriorityElementFromPosition(id, position){
    const elements = await state.getElements()
    for(var index = position-1; index >= 0; index--){
      var comparisonResult = await state.getComparisonResult({id1: id, id2: elements[index]})
      if(comparisonResult == 'lower') return index + 1
    }

    return 0
  }

  async function findLowerPriorityElementFromPosition(id, position){
    const elements = await state.getElements()
    for(var index = position; index < elements.length; index++){
      var comparisonResult = await state.getComparisonResult({id1: id, id2: elements[index]})
      if(comparisonResult == 'greater') return index
    }

    return elements.length
  }

  async function getTheNewElementToInsert(id1, id2){
    var existsElement1 = await existsElement(id1)
    if(existsElement1){
      var existsElement2 = await existsElement(id2)
      return existsElement2? null: id2
    }
    return id1
  }

  async function existsElement(id){
    const elements = await state.getElements()
    return (elements.indexOf(id) != -1)
  }
}

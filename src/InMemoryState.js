module.exports = function(){
  var elements = []
  var waitingForComparison = {}
  var comparisonResults = {}

  return Object.freeze({
    addElement,
    getElements,
    isWaitingForComparison,
    setWaitingForComparison,
    insertElementAt,
    insertElementAfter,
    insertElementBefore,
    setComparisonResult,
    getComparisonResult
  })

  async function addElement(id){
    elements.push(id)
  }

  async function getElements(){
    return elements;
  }

  async function isWaitingForComparison(id1, id2){
    return waitingForComparison[id1][id2]
  }

  async function setWaitingForComparison(id1, id2, waiting){
    await setWaitingForComparisonOneWay(id1, id2, waiting)
    await setWaitingForComparisonOneWay(id2, id1, waiting)
  }

  async function setWaitingForComparisonOneWay(id1, id2, waiting){
    if(!waitingForComparison[id1]){
      waitingForComparison[id1] = {}
    }
    waitingForComparison[id1][id2] = waiting
  }

  async function insertElementAt(id, position){
    elements.splice(position, 0, id)
  }

  async function insertElementAfter(id, existingId){
    var existingIndex = elements.indexOf(existingId)
    await insertElementAt(id, existingIndex+1)
  }

  async function insertElementBefore(id, existingId){
    var existingIndex = elements.indexOf(existingId)
    await insertElementAt(id, existingIndex)
  }

  async function setComparisonResult({id1, id2, result}){
    setComparisonResultOneWay(id1, id2, result)
    setComparisonResultOneWay(id2, id1, await getOpposite(result))
  }

  async function getOpposite(result){
    switch(result){
      case 'lower':
        return 'greater'
      case 'greater':
        return 'lower'
      case 'equal':
        return 'equal'
    }
  }

  async function setComparisonResultOneWay(id1, id2, result){
    if(!comparisonResults[id1]){
      comparisonResults[id1] = {}
    }
    comparisonResults[id1][id2] = result
  }

  async function getComparisonResult({id1, id2}){
    return comparisonResults[id1] ? comparisonResults[id1][id2] : undefined
  }
}

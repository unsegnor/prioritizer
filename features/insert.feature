Feature: Insert elements in the prioritized list

  Scenario: Insert an element in an empty list
    Given the list is empty
    When we insert the element B in the list
    Then the element B must be in the list
    And the list must have only one element

  Scenario: Insert an element in a list with one element
    Given the list has the elements
    |A|
    When we insert the element B in the list
    Then the system must be waiting for the comparison between B and A
    Then the system must notify that is waiting for the comparison between B and A

  Scenario: Insert an element in a list with two elements
    Given the list has the elements
    |A|C|
    When we insert the element B in the list
    Then the system must be waiting for the comparison between B and A

  Scenario: Insert an element in a list with three elements
    Given the list has the elements
    |A|C|D|
    When we insert the element B in the list
    Then the system must be waiting for the comparison between B and C

  Scenario: Insert an element in a list with four elements
    Given the list has the elements
    |A|C|D|E|
    When we insert the element B in the list
    Then the system must be waiting for the comparison between B and C

  Scenario: Insert an element in a list with four elements
    Given the list has the elements
    |A|C|D|E|F|
    When we insert the element B in the list
    Then the system must be waiting for the comparison between B and D

  Scenario: Resolving a comparison
    Given the system is waiting for the comparison between B and A
    When the result of the comparison is that B is greater than A
    Then the system is not waiting for the comparison between B and A
    And there must be a comparison as B is greater than A

  Scenario: Resolving a comparison with the greatest element
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |A|C|D|
    When the result of the comparison is that B is greater than A
    Then the list must be
    |B|A|C|D|

  Scenario: Resolving a comparison with the greatest element in inverse order
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |A|C|D|
    When the result of the comparison is that A is lower than B
    Then the list must be
    |B|A|C|D|

  Scenario: Ignore a comparison result when it is not expected
    Given the system is not waiting for the comparison between B and A
    And the list has the elements
    |A|C|D|
    When the result of the comparison is that B is greater than A
    Then the list must be
    |A|C|D|

  Scenario: Resolving a comparison with the lowest element
    Given the system is waiting for the comparison between A and B
    And the list has the elements
    |C|D|A|
    When the result of the comparison is that B is lower than A
    Then the list must be
    |C|D|A|B|

  Scenario: Resolving a comparison with the lowest element in inverse order
    Given the system is waiting for the comparison between A and B
    And the list has the elements
    |C|D|A|
    When the result of the comparison is that A is greater than B
    Then the list must be
    |C|D|A|B|

  Scenario: Resolving a comparison when the element has the same priority
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |C|A|D|
    When the result of the comparison is that B is equal than A
    Then the list must be
    |C|A|B|D|

  Scenario: Resolving a comparison when the element has the same priority in inverse order
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |C|A|D|
    When the result of the comparison is that A is equal than B
    Then the list must be
    |C|A|B|D|

  Scenario: Resolving a comparison when there are greater elements to compare with
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |C|A|D|
    And the element B has not been yet compared with C
    When the result of the comparison is that B is greater than A
    Then the system must be waiting for the comparison between B and C

  Scenario: Resolving a comparison when there are lower elements to compare with
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |D|A|C|
    And the element B has not been yet compared with C
    When the result of the comparison is that B is lower than A
    Then the system must be waiting for the comparison between B and C

  Scenario: Resolving a comparison when there are greater elements already compared
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |C|A|D|
    And the element B has already been compared as lower than C
    When the result of the comparison is that B is greater than A
    Then the list must be
    |C|B|A|D|

  Scenario: Resolving a comparison when there are lower elements already compared
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |D|A|C|
    And the element B has already been compared as greater than C
    When the result of the comparison is that B is lower than A
    Then the list must be
    |D|A|B|C|

  Scenario: Resolving a comparison when there is one element between the last comparison and the next greater compared
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |C|D|A|E|
    And the element B has already been compared as lower than C
    And the element B has not been yet compared with D
    When the result of the comparison is that B is greater than A
    Then the system must be waiting for the comparison between B and D

  Scenario: Resolving a comparison when there is one element between the last comparison and the next lower compared
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |E|A|D|C|
    And the element B has already been compared as greater than C
    And the element B has not been yet compared with D
    When the result of the comparison is that B is lower than A
    Then the system must be waiting for the comparison between B and D

  Scenario: Resolving a comparison when there are two elements between the last comparison and the next greater compared
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |C|D|E|A|F|
    And the element B has already been compared as lower than C
    And the element B has not been yet compared with D
    When the result of the comparison is that B is greater than A
    Then the system must be waiting for the comparison between B and D

  Scenario: Resolving a comparison when there are two elements between the last comparison and the next lower compared
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |F|A|D|E|C|
    And the element B has already been compared as greater than C
    And the element B has not been yet compared with D
    When the result of the comparison is that B is lower than A
    Then the system must be waiting for the comparison between B and D

  Scenario: Resolving a comparison when there are three elements between the last comparison and the next greater compared
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |C|D|E|F|A|G|
    And the element B has already been compared as lower than C
    And the element B has not been yet compared with E
    When the result of the comparison is that B is greater than A
    Then the system must be waiting for the comparison between B and E

  Scenario: Resolving a comparison when there are three elements between the last comparison and the next lower compared
    Given the system is waiting for the comparison between B and A
    And the list has the elements
    |G|A|D|E|F|C|
    And the element B has already been compared as greater than C
    When the result of the comparison is that B is lower than A
    And the element B has not been yet compared with E
    Then the system must be waiting for the comparison between B and E

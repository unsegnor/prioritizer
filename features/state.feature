Feature: Keep state

  Scenario: Keep the pending comparisons
    Given the system is waiting for the comparison between B and A
    Then the system must be waiting for the comparison between B and A

  Scenario: Do not invent the pending comparisons
    Given the system is not waiting for the comparison between B and A
    Then the system must not be waiting for the comparison between B and A

  Scenario: Pending comparisons elements order is not considered
    Given the system is waiting for the comparison between B and A
    Then the system must be waiting for the comparison between A and B

  Scenario: Keep the elements
    Given the list has the elements
    |A|
    Then the element A must be in the list

  Scenario: Keep several elements
    Given the list has the elements
    |A|B|C|
    Then the element B must be in the list
    And the element C must be in the list
    And the element A must be in the list
    And the element Z must not be in the list

  Scenario: Insert element in position 0
    Given the list has the elements
    |A|B|C|
    When we insert the element D in the position 0
    Then the list must be
    |D|A|B|C|

  Scenario: Insert element in position greater than 0
    Given the list has the elements
    |A|B|C|
    When we insert the element D in the position 2
    Then the list must be
    |A|B|D|C|

  Scenario: Insert element in the last position
    Given the list has the elements
    |A|B|C|
    When we insert the element D in the position 3
    Then the list must be
    |A|B|C|D|

  Scenario: Insert element after another element
    Given the list has the elements
    |A|B|C|
    When we insert the element D after the element B
    Then the list must be
    |A|B|D|C|

  Scenario: Insert element before another element
    Given the list has the elements
    |A|B|C|
    When we insert the element D before the element B
    Then the list must be
    |A|D|B|C|

  Scenario: Do not invent results
    Given the element B has not been yet compared with C
    Then there must not be any comparison between B and C

  Scenario: Persist comparison results lower
    Given the element B has already been compared as lower than C
    Then there must be a comparison as B is lower than C

  Scenario: Persist comparison results greater
    Given the element B has already been compared as greater than C
    Then there must be a comparison as B is greater than C

  Scenario: Persist comparison results equal
    Given the element B has already been compared as equal than C
    Then there must be a comparison as B is equal than C
    
  Scenario: Persist comparison results equal swapped
    Given the element B has already been compared as equal than C
    Then there must be a comparison as C is equal than B

  Scenario: Persist comparison results lower swapped
    Given the element B has already been compared as lower than C
    Then there must be a comparison as C is greater than B

  Scenario: Persist comparison results greater swapped
    Given the element B has already been compared as greater than C
    Then there must be a comparison as C is lower than B

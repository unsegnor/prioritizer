const { Given, When, Then } = require('cucumber')
const {assert} = require('chai')

Given('the list is empty', function () {
})

When(/^we insert the element (.*) in the list$/, async function (elementId) {
  await this.system.insertElement(elementId)
})

Then(/^the element (.*) must be in the list$/, async  function (elementId) {
  const elements = await this.system.getElements()
  assert.deepInclude(elements, elementId)
})

Then(/^the element (.*) must not be in the list$/, async  function (elementId) {
  const elements = await this.system.getElements()
  assert.notDeepInclude(elements, elementId)
})

Then('the list must have only one element', async function () {
  const elements = await this.system.getElements()
  assert.strictEqual(1, elements.length)
})

Given('the list has the elements', async function (dataTable) {
  for(var id of dataTable.rawTable[0]){
    await this.state.addElement(id)
  }
 })

 Then(/^the system must be waiting for the comparison between (.*) and (.*)$/, async function (id1, id2) {
   var isWaiting = await this.state.isWaitingForComparison(id1, id2)
   assert.isOk(isWaiting)
 })

 Then(/^the system must notify that is waiting for the comparison between (.*) and (.*)$/, async function (id1, id2) {
   var isNotified = await this.notifier.hasBeenNotifiedTheSystemIsWaitingForTheComparison({id1, id2})
   assert.isOk(isNotified)
 })

 Then(/^the system must not be waiting for the comparison between (.*) and (.*)$/, async function (id1, id2) {
   var isWaiting = await this.state.isWaitingForComparison(id1, id2)
   assert.isNotOk(isWaiting)
 })

 Given(/^the system is waiting for the comparison between (.*) and (.*)$/, async function (id1, id2) {
   await this.state.setWaitingForComparison(id1, id2, true)
 })

 Given(/^the system is not waiting for the comparison between (.*) and (.*)$/, async function (id1, id2) {
   await this.state.setWaitingForComparison(id1, id2, false)
 })

 When(/^the result of the comparison is that (.*) is (greater|lower|equal) than (.*)$/, async function (id1, result, id2) {
   await this.system.setComparisonResult({id1, result, id2})
 })

 Then('the list must be', async function (dataTable) {
   const list = await this.state.getElements()
   const expectedList = dataTable.rawTable[0]
   assert.strictEqual(list.length, expectedList.length)
   for(var index in expectedList){
     assert.strictEqual(list[index], expectedList[index])
   }
 })

 When(/^we insert the element (.*) in the position (.*)$/, async function (id1, position) {
   await this.state.insertElementAt(id1, position)
 })

 When(/^we insert the element (.*) after the element (.*)$/, async function (insertedId, existingId) {
   await this.state.insertElementAfter(insertedId, existingId)
 })

 When(/^we insert the element (.*) before the element (.*)$/, async function (insertedId, existingId) {
   await this.state.insertElementBefore(insertedId, existingId)
 })

 Given(/^the element (.*) has not been yet compared with (.*)$/, async function (id1, id2) {
 })

 Given(/^the element (.*) has already been compared as (greater|lower|equal) than (.*)$/, async function (id1, result, id2) {
   await this.state.setComparisonResult({id1, id2, result})
 })

 Then(/^there must not be any comparison between (.*) and (.*)$/, async function (id1, id2) {
   var comparisonResult = await this.state.getComparisonResult({id1, id2})
   assert.strictEqual(undefined, comparisonResult)
 })

 Then(/^there must be a comparison as (.*) is (lower|greater|equal) than (.*)$/, async function (id1, expectedResult, id2) {
   var comparisonResult = await this.state.getComparisonResult({id1, id2})
   assert.strictEqual(expectedResult, comparisonResult)
 })


//---------------------------------------------------------

When('we insert a new element', async function () {
  this.createNewElement()
  await this.system.insertElement({element: this.element})
})

Then('the new element is in the list', async function () {
  const elements = await this.system.getElements()
  assert.include(elements, this.element)
})

Given('there is an element in the list', async function () {
  this.existingElement = this.getNewElement()
  await this.state.addElement({element: this.existingElement})
})

When('we insert a new element with less priority', async function () {
  this.createNewElement()
  this.comparer.setPriority(this.element).lessThan(this.existingElement)
  await this.system.insertElement({element: this.element})
})

Then('the new element is the element with lower priority', async function () {
  const elements = await this.system.getElements()
  assert.deepEqual(elements[1], this.element)
})

const { setWorldConstructor } = require('cucumber')
const System = require('../../src/System')
const State = require('../../src/InMemoryState')
const Notifier = require('../../adapters/Notifier/FakeNotifier')

class CustomWorld {
  constructor() {
    this.lastElementIndex = 0
    this.elementB = this.getNewElement()
    this.state = State()
    this.notifier = Notifier()
    this.system = System({state: this.state, notifier: this.notifier})
  }

  createNewElement(){
    this.element = this.getNewElement()
  }

  getNewElement(){
    this.lastElementIndex++
    return {id: this.lastElementIndex}
  }
}

setWorldConstructor(CustomWorld)

module.exports = function(){
  let notifications = {}

  return Object.freeze({
    notifySystemIsWaitingForComparison,
    hasBeenNotifiedTheSystemIsWaitingForTheComparison
  })

  async function notifySystemIsWaitingForComparison({id1, id2}){
    notifications[id1] = notifications[id1] || {}
    notifications[id1][id2] = true
  }

  async function hasBeenNotifiedTheSystemIsWaitingForTheComparison({id1, id2}){
    return (notifications[id1] != undefined) && (notifications[id1][id2] == true)
  }
}

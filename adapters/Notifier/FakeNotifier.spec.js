const Notifier = require('./FakeNotifier')
const Port = require('../../ports/Notifier')
const {assert} = require('chai')

describe('FakeNotifier', function(){
  beforeEach(function(){
    this.adapter = Notifier()
  })

  Port()

  describe('Assert methods', function(){
    const id1 = 'id1'
    const id2 = 'id2'

    it('must indicate when a notification has not been made', async function(){
      const hasBeenNotified = await this.adapter.hasBeenNotifiedTheSystemIsWaitingForTheComparison({id1, id2})
      assert.strictEqual(false, hasBeenNotified)
    })

    it('must indicate when a notification has been made', async function(){
      await this.adapter.notifySystemIsWaitingForComparison({id1, id2})
      const hasBeenNotified = await this.adapter.hasBeenNotifiedTheSystemIsWaitingForTheComparison({id1, id2})
      assert.strictEqual(true, hasBeenNotified)
    })

    it('must indicate when a notification has not been made with the second element', async function(){
      await this.adapter.notifySystemIsWaitingForComparison({id1, id2})
      const hasBeenNotified = await this.adapter.hasBeenNotifiedTheSystemIsWaitingForTheComparison({id1, id2: 'id3'})
      assert.strictEqual(false, hasBeenNotified)
    })
  })
})
